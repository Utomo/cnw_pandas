
# Task List

---

## Laporan Burger king rekap

**Request** : Jessica
**Scope** : Multicompany ( Luar kota)
**Assign** : Nurdin

**Description**

Laporan Piutang seluruh customer ```Burger King``` di semua group Indoguna.

Informasi : 

|Company | Invoice | Kwitansi | Tanggal TF | Tanggal Due | Total balance |
| --- | --- | --- | --- |  --- | --- |


## Training TF , Piutang,  & kartu Piutang

**Request** : AR
**Scope** : Multicompany ( Luar kota)
**Assign** : Dika, Imam, Rio

**Description**
Training ulang laporan piutang dengan case :

_Saldo Piutang Detail_

1. Lihat jatuh tempo untuk tanggal tertentu
2. Lihat invoice yang belum di tukar faktur
3. Lihat invoice yang belum dibuat kwitansi dan belum tukar faktur
4. Laporan jadwal harian
5. cara update collector
6. cara update jadwal tukar faktur , dan kompoen lain
7. cek summary piutang yang akan jatuh tempo ( Proyeksi summary)
8. Lihat piutang selain piutang dagang ( DN, PPh23)
9. Lihat pembayaran yang belum di reconsile

_Kartu Piutang_

1. Lihat pembayaran customer per tanggal
2. Lihat pembayaran yang belum di reconsile
3. cara lihat total pembayaran tertentu ( checklist )
4. rekap pembayaran custoemr


_Lap DO belum jadi invoice_

1. Laporan DO belum jadi invoice per AR

_Lap Invoice belum dibuat kwitansi_

1. Laporan invoice yang belum dibuat kwitansi berdasarkan customer


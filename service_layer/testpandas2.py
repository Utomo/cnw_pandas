import pandas as pd
import pandas.io.sql
import psycopg2 as pg 
import numpy as np
from jinja2 import Environment, FileSystemLoader
import pdfkit

env = Environment(loader=FileSystemLoader('/data/igu_it_dev/cnw_awr28/testing_python/'))
template = env.get_template("template.html")

datacompany = ({'host':'192.168.1.6' , 'dbname':'TOKOTANI-BEKASITIMUR','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-BINTARA','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-BOGOR','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CENGKARENG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CIKARANG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CINERE','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-KALIDERES','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-KEDOYA','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-PAMULANG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-PANGKALANJATI','user':'tokotani','password':'tokotani'},)
datalist=[]

path_HTMLfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/myexcel3a.html"
path_PDFfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/tti.pdf"
path_XLSfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/tti.xls"
path_PDFfile2 = "/data/igu_it_dev/cnw_awr28/testing_python/myexcel3b.pdf"
cssfile = "/data/igu_it_dev/cnw_awr28/testing_python/style.css"
 
for company in datacompany:
    msg_sql= """
                    select  '""" + company["dbname"] + """' as company,
                            d.name product_name, 
                            to_Char(a.date_order,'yyyy-MM-dd')	date_order,
                            left(to_Char(a.date_order,'yyyy-MM-dd'),7)	month_order,
                            sum(b.qty)::float quantity,
                            sum(b.qty * b.price_unit)::float  amount 
                    from pos_order a
                        inner join pos_order_line b on a.id = b.order_id 
                        inner join product_product c on b.product_id = c.id
                        inner join product_template d on c.product_tmpl_id = d.id 
                    where a.date_order>='2019-01-01' and a.date_order<='2019-12-31'
                    group by d.name , 
                        to_Char(a.date_order,'yyyy-MM-dd')
                    order by date_order
            """    
    conn = pg.connect("host="+ company["host"] + " dbname="+ company["dbname"] + " user="+ company["user"] + " password="+ company["password"] + "")
    
    data = pd.io.sql.read_sql(msg_sql,conn)
    datalist.append(data)
  
final = pd.concat(datalist) 
  
tti_report = final.pivot_table(index=["month_order"],columns=["company"],aggfunc=np.sum,  values=["amount"],fill_value="0",margins=True )
 
template_vars = {"title" : "Penjualan Tokotani",
                 "tti_report": tti_report.to_html(float_format='{:20,.2f}'.format)}

options = {
            'page-size': 'legal',
            'orientation': 'landscape',
            }
html_out = template.render(template_vars)

pdfkit.from_string(html_out,path_PDFfile1,options=options,css=cssfile)  

tti_report.to_excel(path_XLSfile1)
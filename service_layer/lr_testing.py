import pandas as pd
import pandas.io.sql
import pymssql as sapsql 
import numpy as np
from jinja2 import Environment, FileSystemLoader
import pdfkit

env = Environment(loader=FileSystemLoader('/data/igu_it_dev/cnw_awr28/testing_python/'))
template = env.get_template("template.html")

datacompany = ({'host':'192.168.1.13' , 'dbname':'IGU_LIVE','user':'sa','password':'B1admin'},)

datalist=[]

path_XLSfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/lr_igu.xls"
path_HTMLfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/lr_igu.html"
path_PDFfile1 = "/data/igu_it_dev/cnw_awr28/testing_python/lr_igu.pdf"
path_PDFfile2 = "/data/igu_it_dev/cnw_awr28/testing_python/lr_igu2.pdf"
cssfile = "/data/igu_it_dev/cnw_awr28/testing_python/style.css"
 
for company in datacompany:
    msg_sql= """
                exec [dbo].[IGU_LR_PERITEM] '20200101', '20200430','IGU' """

    conn = sapsql.connect(host=company["host"] ,dbname= company["dbname"] ,user=company["user"] ,password=company["password"])
    
    data = pandas.io.sql.read_sql(msg_sql,conn)
    datalist.append(data)

final = pd.concat(datalist) 
  
tti_report = final.pivot_table(index=["date_order"],columns=["company"],aggfunc=np.sum,  values=["amount"],fill_value="0",margins=True )
 
template_vars = {"title" : "Penjualan Tokotani",
                 "tti_report": tti_report.to_html(float_format='{:20,.2f}'.format)}

options = {
            'page-size': 'legal',
            'orientation': 'landscape',
            }
html_out = template.render(template_vars)

pdfkit.from_string(html_out,path_PDFfile1,options=options,css=cssfile) 
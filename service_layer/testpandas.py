import pandas as pd
import pandas.io.sql
import psycopg2 as pg 
import numpy as np


# -*- coding: utf-8 -*-
datacompany = ({'host':'192.168.1.6' , 'dbname':'TOKOTANI-BEKASITIMUR','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-BINTARA','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-BOGOR','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CENGKARENG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CIKARANG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-CINERE','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-KALIDERES','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-KEDOYA','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-PAMULANG','user':'tokotani','password':'tokotani'},
                {'host':'192.168.1.6' , 'dbname':'TOKOTANI-PANGKALANJATI','user':'tokotani','password':'tokotani'},)
datalist=[]

path_excelfile = "/data/igu_it_dev/cnw_awr28/testing_python/myexcel2.xls"

for company in datacompany:
    msg_sql= """
                    SELECT '""" + company["dbname"] + """' AS company,
                                c.name journal, 
                                TO_CHAR(a.date,'yyyy-MM-dd') date_order,
                                sum(a.amount)amount
                    FROM account_move a 
                        INNER JOIN account_journal c ON a.journal_id = c.id 
                    WHERE a.date >= '2020-04-01'
                        AND a.state ='posted' 
                        AND c.name <>'Stock Journal'
                    GROUP BY    c.name  , 
                                TO_CHAR(a.date,'yyyy-MM-dd') 
                    ORDER BY    TO_CHAR(a.date,'yyyy-MM-dd') , 
                                c.name  
            """    
    conn = pg.connect("host="+ company["host"] + " dbname="+ company["dbname"] + " user="+ company["user"] + " password="+ company["password"] + "")
    data = pandas.io.sql.read_sql(msg_sql,conn)
    datalist.append(data)

final = pd.concat(datalist) 


final[(final['journal']!='POS Sale Journal')].pivot_table(index=["company","date_order"], columns="journal",values="amount",aggfunc=[np.sum],fill_value=0.0,margins=True ).to_excel(path_excelfile)

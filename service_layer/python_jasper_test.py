import os
from pyreportjasper import PyReportJasper
 
RESOURCES_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'datatest')
REPORTS_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'datatest')
input_file = os.path.join(REPORTS_DIR, 'test_report.jrxml')
data_file = os.path.join(REPORTS_DIR, 'test_report.json')
output_file = os.path.join(REPORTS_DIR, 'json')
conn = {
    'driver': 'json',
    'data_file': data_file,
    'json_query': ''
}
pyreportjasper = PyReportJasper()
pyreportjasper.config(
    input_file,
    output_file,
    output_formats=["pdf","xls"],
    db_connection=conn
)
pyreportjasper.process_report()
print('Result is the file below.')


print(output_file + '.pdf')
